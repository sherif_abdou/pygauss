from typing import List
from random import randint
from time import time


def take_while(arr, cond):
    new_arr = []
    for v in arr:
        if not cond(v):
            break
        new_arr.append(v)
    return new_arr


def zeroes(row):
    return len(take_while(row, lambda x: x == 0))


def row_proper(row, i, row_len):
    if len(take_while(row, lambda x: x == 0)) != row_len-i-2:
        return False
    return True


def is_row_eschelon(matrix: List[List[float]]):
    if len(matrix) == 0:
        return True
    row_len = len(matrix[0])
    for i, row in enumerate(matrix):
        if not row_proper(row, i, row_len):
            return False
    return True


def swap_arr(arr, a, b):
    tmp = arr[a]
    arr[a] = arr[b]
    arr[b] = tmp


def add_row(a, b):
    return [c+d for c, d in zip(a, b)]


def multiply_row(k, row):
    return [k*v for v in row]


def round_matrix(matrix, p):
    for i in range(len(matrix)):
        row = matrix[i]
        for j in range(len(row)):
            matrix[i][j] = round(matrix[i][j], p)
    return matrix


def solve(matrix, p = 4):
    if is_row_eschelon(matrix):
        for i in range(len(matrix[:-1])):
            row = matrix[i]
            for j in range(len(matrix[i+1:])):
                n_row = matrix[i+1+j]
                n = i+1
                to_remove = n_row[-(1+n)]
                orig = row[-(1+n)]
                coef = -(to_remove/orig)
                matrix[j+i+1] = add_row(n_row, multiply_row(coef, row))
        for i in range(len(matrix)):
            row = matrix[i]
            matrix[i] = multiply_row(1/row[-(i+2)],row)
        return round_matrix(matrix, p)
    else:
        for i in range(len(matrix[:-1])):
            row = matrix[i]
            early = False
            for j in range(len(matrix[i+1:])):
                n_row = matrix[i+1+j]
                n = i+1
                to_remove = n_row[i]
                orig = row[i]
                if orig == 0:
                    continue
                coef = -(to_remove/orig)
                before = zeroes(n_row)
                matrix[j+i+1] = add_row(n_row, multiply_row(coef, row))
                after = zeroes(matrix[j+i+1])
                if after-before > 1:
                    early = True
            if early:
                new_matrix = round_matrix(sorted(matrix, key=lambda x: zeroes(x), reverse=True), p)
                return solve(new_matrix)
        matrix.reverse()
        return solve(round_matrix(matrix, p))

def random_matrix(size):
    matrix = []
    for i in range(size):
        row = []
        for j in range(size+1):
            row.append(randint(1, 500))
        matrix.append(row)
    return matrix


def matrix_from_user():
    size = int(input("How many variables? "))
    matrix = []
    for i in range(size):
        row = []
        for j in range(size+1):
            v = float(input(f"Input spot (Row: {i+1}, Column: {j+1}): "))
            row.append(v)
        matrix.append(row)
    return matrix


def nice_print_solved(matrix):
    matrix.reverse()
    for row in matrix:
        i = 0
        while row[i] == 0:
            i += 1
        print(f"Variable #{i+1} = {row[-1]}")


if __name__ == "__main__":
    """
    m = [[0, 0, 1, 5], [0, 1, 3, 6], [1, 4, 8, 12]]
    om = [[5, 2, 20], [20, 0, 42]]
    """
    # user = matrix_from_user()
    user = matrix_from_user()
    print("\n")
    start = time()
    solved = solve(user)
    end = time()
    nice_print_solved(solved)
    print(f"Time taken: {round(end-start, 4)} seconds")
